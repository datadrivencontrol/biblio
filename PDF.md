﻿Revistas que permitem publicação de artigos PDF



Journal of Process Control
Colocar DOI link
© <year>. This manuscript version is made available under the CC-BY-NC-ND 4.0 license 
http://creativecommons.org/licenses/by-nc-nd/4.0/ 


Journal of Control, Automation and Electrical Systems
The final publication is available at Springer via http://dx.doi.org/[insert DOI]”."

Automatica
Imediatamente no blog
Colocar DOI link
© <year>. This manuscript version is made available under the CC-BY-NC-ND 4.0 license 
http://creativecommons.org/licenses/by-nc-nd/4.0/ 

Proceedings of the Institution of Mechanical Engineers. Part I, Journal of Systems and Control Engineering
fora universidade 12 mese
When posting or re-using the article please provide a link to the appropriate DOI for the published version of the article on SAGE Journals (http://online.sagepub.com)


International Journal of Systems Science
For example: “This is an Accepted Manuscript of an article published by Taylor & Francis Group in Africa Review on 17/04/2014, available online: http://www.tandfonline.com/10.1080/12345678.1234.123456.

IET Control Theory & Applications
"This paper is a postprint of a paper submitted to and accepted for publication in [journal] and is subject to Institution of Engineering and Technology Copyright. The copy of record is available at IET Digital Library"

Journal of the Franklin Institute
Colocar DOI link
© <year>. This manuscript version is made available under the CC-BY-NC-ND 4.0 license 
http://creativecommons.org/licenses/by-nc-nd/4.0/ 


IEEE

© 20xx IEEE.Personal use of this material is permitted. Permission from IEEE must be 
obtained for all other uses, in any current or future media, including reprinting
/republishing this material for advertising or promotional purposes,creating new 
collective works, for resale or redistribution to servers or lists, or reuse of any 
copyrighted component of this work in other works.

The final publication is available at IEEE via http://dx.doi.org/[insert DOI]”."





Congressos IEEE

Colocar a versao enviada, sem necessidade de textos.